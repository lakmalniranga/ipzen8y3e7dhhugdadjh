## Installation

(Use your fav package manager)

1. Clone repo

2. Install dependencies

    ```yarn install```

3. Development mode
    
    terminal tab1 
    ```yarn dev:build```
    
    terminal tab2
    ```yarn dev```

4. Production mode
    
    ```yarn start```

## Usage
 
1. Get IP
    
    json - http://host:port/ip?format=json

    text - http://host:port/ip || http://host:port/ip?format=json

2. Frontend
    
    visit - http://host:port

## Tech use

I use the latest feature of javascript. ES6 + ES7, we compile with the help of babel and webpack v2.
