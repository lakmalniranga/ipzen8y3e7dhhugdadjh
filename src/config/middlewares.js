import express from 'express';
import morgan from 'morgan';
import bodyParser from 'body-parser';
import compression from 'compression';
import helmet from 'helmet';
import path from 'path';

const isDev = process.env.NODE_ENV === 'development';
const isProd = process.env.NODE_ENV === 'production';

export default app => {
  if (isProd) {
    app.use(compression());
    app.use(helmet());
  }

  // Enable trust proxy to get accurate ip address of client
  app.enable('trust proxy');
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));

  // TODO: uncomment after placing your favicon in /public
  // view engine setup
  app.set('views', path.resolve('src/views'));
  app.set('view engine', 'ejs');
  //app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
  app.use(express.static(path.join(__dirname, 'views')));

  if (isDev) {
    app.use(morgan('dev'));
  }
};
