export function index(req, res) {
  // get ip form x-forwarded-for header if available
  // or get from client's request
  const ip = req.ip || req.connection.remoteAddress;
  res.render('index', { ip: ip });
}
