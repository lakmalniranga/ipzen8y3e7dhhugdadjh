import { Router } from 'express';

import * as viewController from './view.controllers';

const routes = new Router();

routes.get('/', viewController.index);

export default routes;
