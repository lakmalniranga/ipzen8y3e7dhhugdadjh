import { Router } from 'express';

import * as ipController from './ip.controllers';

const routes = new Router();

routes.get('/', ipController.clientIp);

export default routes;
