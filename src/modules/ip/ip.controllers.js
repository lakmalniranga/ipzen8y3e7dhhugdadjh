export function clientIp(req, res) {
  try {
    const format = req.query.format;

    // get ip form x-forwarded-for header if available
    // or get from client's request
    const ip = req.ip || req.connection.remoteAddress;

    // Check user requested format of response
    switch (format) {
      case 'json':
        return res.json({ ip: ip });
        break;
      
      case 'text':
        return res.send(ip);
        break;
    
      default:
        return res.send(ip);
    }
  } catch (e) {
    return res.status(400).json({
      error: "Can't find IP address",
    });
  }
}
